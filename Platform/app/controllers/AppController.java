package controllers;
import play.mvc.*;
import play.api.data.*;
import play.api.data.Form;
import play.api.data.Forms.*;
import javax.inject.Inject;
import play.api.data.validation.Constraints.*;

import org.h2.engine.User;
import org.mindrot.jbcrypt.BCrypt;

import models.Users;

import play.data.*;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import views.html.*;
import views.html.helper.form;

public class AppController extends Controller {
	
	
	@Inject

	
	FormFactory formFactory;

	
	
	
	


	public Result index() {
	    return ok(
	        "ok");
	}
	
	public Result login(){
		
		
		return ok(login.render(formFactory.form(Login.class)));
		
	
	}
	
	public Result authenticate() {
	    play.data.Form<Login> loginForm =formFactory.form(Login.class).bindFromRequest();
	    if (loginForm.hasErrors()) {
	        return badRequest(login.render(loginForm));
	    } else {
	       session().clear();
	       session("email", loginForm.get().email);
	       // return redirect(
	         //   routes.AppController.index());
	      Users user= Users.find.where().eq("email",loginForm.get().email).findUnique();
	        return ok(index.render("Welome "+user.name));
	    }
	}	


	//to create a user

	public Result create()
	{play.data.Form<Users> RegistrationForm=formFactory.form(Users.class);
		
	return ok(Registration.render(RegistrationForm));}

	//to save a user
	public Result save()
	{play.data.Form<Users> RegistrationForm=formFactory.form(Users.class).bindFromRequest();
	/*
	if(RegistrationForm.hasErrors()) {
		flash("danger","Please Correct the Form Below ");
		return badRequest(Registration.render(RegistrationForm));
	}
*/
	//check password confirmation
			 
				if(!RegistrationForm.field("password").valueOr("")
						.equals(RegistrationForm.field("passwordConfirm").value())) {
					//RegistrationForm.reject("passwordConfirm","Password don't match");
					flash("danger","Password don't match ");
					return badRequest(Registration.render(RegistrationForm));
				}
				
			
    Users user =RegistrationForm.get();
    
    if((user.find.where().eq("email", RegistrationForm.field("email").value()).findUnique()) != null)
    {
    	flash("danger","account already exists try to sign in ");	
    	return badRequest(Registration.render(RegistrationForm));
    }
    
    user.password = BCrypt.hashpw(RegistrationForm.field("password").value(), BCrypt.gensalt());
	user.save();
	flash("success","user Saved Successfully");
	 return ok(index.render("Thanks for registring "+user.name+" a confirmation email will be sent to you shortly"));
	//return redirect(routes.AppController.index());


	}
	public static class Login{	
	
		public String email;
	    public String password;
		public String getEmail() {
			return email;
		}
		public String getPassword() {
			return password;
		}
		
		public void setEmail(String email) {
			this.email = email;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
	
		public String validate() {
		    if (Users.authenticate(email, password) == null) {
		      return "Invalid user or password";
		    }
		    return null;
		}
	
	
	}
	
	
	
	
}


	
	
	


